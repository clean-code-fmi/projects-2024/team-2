package org.account;

import org.junit.jupiter.api.Test;
import org.utils.Height;
import org.utils.HeightUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountTest {
    @Test
    void whenSettingFullNameExpectCorrectlySetNames() {
        Account account = new Account();
        account.setName(new String[] {"Ivan", "Petrov"});

        assertEquals("Ivan", account.getFirstName());
        assertEquals("Petrov", account.getLastName());
    }

    @Test
    void whenSettingDetailsExpectCorrectlySetDetails() {
        Account account = new Account();
        account.setAge(18);
        account.setWeight(55);
        account.setHeight(new Height(HeightUnit.CENTIMETERS, 165));

        assertEquals(account.getAge(), 18);
        assertEquals(account.getWeight(), 55);
        assertEquals(account.getHeight().getHeightUnit().getFactor(), 1);
        assertEquals(account.getHeight().getHeightValue(), 165);
    }
}
