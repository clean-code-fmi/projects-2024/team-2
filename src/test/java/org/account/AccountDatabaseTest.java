package org.account;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccountDatabaseTest {
    @Test
    void whenRegisteringAccountExpectAccountDatabaseToAddIt() {
        AccountDatabase db = new AccountDatabase();
        Account account = Mockito.mock(Account.class);

        db.addAccount(account);

        assertTrue(db.getAccounts().contains(account));
    }
}
