package org.account;

import org.cli.RegistrationMenu;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class AccountOrchestratorTest {
    @Test
    void whenRegisteringAccountExpectAddToDatabase() {
        AccountDatabase db = Mockito.mock(AccountDatabase.class);
        RegistrationMenu menu = Mockito.mock(RegistrationMenu.class);
        Account account = Mockito.mock(Account.class);
        AccountOrchestrator orchestrator = new AccountOrchestrator(db, menu);

        Mockito.when(menu.getRegistration())
                .thenReturn(account);

        orchestrator.register();

        Mockito.verify(db)
                .addAccount(account);
    }
}
