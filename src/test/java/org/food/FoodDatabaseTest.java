package org.food;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FoodDatabaseTest {

    private final Food testFood = new Food("Apple", "unit", 10, 20, 30);

    @Test
    void afterAddingFoodInFoodDatabaseExpectToBeContained() {
        FoodDatabase foodDatabase = new FoodDatabase();
        foodDatabase.addFood(testFood);
        Assertions.assertTrue(foodDatabase.foodExists(testFood.getName()));
    }

    @Test
    void whenCheckingWhetherFoodWhichIsNotPresentExistsExpectFalse() {
        FoodDatabase foodDatabase = new FoodDatabase();
        foodDatabase.addFood(testFood);
        Assertions.assertFalse(foodDatabase.foodExists("NotExist"));
    }



    @Test
    void whenGettingFoodWhichExistsExpectToGetExactlyTheWantedFood() {

        FoodDatabase foodDatabase = new FoodDatabase();

        foodDatabase.addFood(testFood);
        Food food = foodDatabase.getFood(testFood.getName());

        Assertions.assertEquals(testFood.getName(), food.getName(),
                "Obtained food has changed or different name");
        Assertions.assertEquals(testFood.getDosage(), food.getDosage(),
                "Obtained food has changed or different dosage");
        Assertions.assertEquals(testFood.getCalories(), food.getCalories(),
                "Obtained food has changed or different calories");
        Assertions.assertEquals(testFood.getFats(), food.getFats(),
                "Obtained food has changed or different fats");
        Assertions.assertEquals(testFood.getWeight(),
                food.getWeight(),
                "Obtained food has changed or different weight");
        Assertions.assertEquals(1, foodDatabase.getAllFoods().size(),
                "Unexpectedly deleted or added an item in food database");
    }

    @Test
    void whenGettingFoodWhichDoesNotExistExpectToGetNull() {
        FoodDatabase foodDatabase = new FoodDatabase();
        Assertions.assertNull(foodDatabase.getFood("NotExist"));
    }

    @Test
    void whenDeletingFoodWhichDoesNotExistExpectToGetNullAndUnchangedMap() {

        FoodDatabase foodDatabase = new FoodDatabase();
        foodDatabase.addFood(testFood);

        Food deletedFood = foodDatabase.removeFood("Not exist");

        Assertions.assertNull(deletedFood);
        Assertions.assertEquals(1, foodDatabase.getAllFoods().size(),
                "Unexpectedly deleted or added an item in food database");
    }

    @Test
    void whenDeletingFoodWhichExistsExpectToGetFoodReferenceAndReducedSize() {

        FoodDatabase foodDatabase = new FoodDatabase();
        foodDatabase.addFood(testFood);

        Food food = foodDatabase.removeFood(testFood.getName());

        Assertions.assertEquals(testFood.getName(), food.getName(),
                "Deleted food has changed or different name");
        Assertions.assertEquals(testFood.getDosage(), food.getDosage(),
                "Deleted food has changed or different dosage");
        Assertions.assertEquals(testFood.getCalories(), food.getCalories(),
                "Deleted food has changed or different calories");
        Assertions.assertEquals(testFood.getFats(), food.getFats(),
                "Deleted food has changed or different fats");
        Assertions.assertEquals(testFood.getWeight(),
                food.getWeight(),
                "Deleted food has changed or different weight");
        Assertions.assertEquals(0, foodDatabase.getAllFoods().size(),
                "Deletion of food in database had no effect.");
    }

}
