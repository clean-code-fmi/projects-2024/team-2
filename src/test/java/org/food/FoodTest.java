package org.food;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FoodTest {

    private final Food testFood = new Food("Apple", "unit", 10, 20, 30, 4);

    @Test
    void whenCreatingFoodWithNegativeCaloriesExpectException() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Food("Apple", "unit", -5, 10, 10));
    }

    @Test
    void whenCreatingFoodWithNegativeFatsExpectException() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Food("Apple", "unit", 10, -5, 10));
    }

    @Test
    void whenCreatingFoodWithNegativeWeightExpectException() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Food("Apple", "unit", 10, 10, -5));
    }

    @Test
    void whenCreatingFoodWithNegativeOrZeroQuantityExpectException() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Food("Apple", "unit", 10, 10, 10, -5));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Food("Apple", "unit", 10, 10, 10, 0));
    }


    @Test
    void calculationOfTotalCaloriesIsCorrect() {
        Assertions.assertEquals(40, testFood.getTotalCalories());
    }

    @Test
    void calculationOfTotalFatsIsCorrect() {
        Assertions.assertEquals(80, testFood.getTotalFats());
    }

    @Test
    void calculationOfTotalWeightIsCorrect() {
        Assertions.assertEquals(120, testFood.getTotalWeight());
    }

    @Test
    void calculationOfCaloriesPerOneQuantityIsCorrect() {
        Assertions.assertEquals(10, testFood.getCalories());
    }

    @Test
    void calculationOfFatsPerOneQuantityIsCorrect() {
        Assertions.assertEquals(20, testFood.getFats());
    }

    @Test
    void calculationOfWeightPerOneQuantityIsCorrect() {
        Assertions.assertEquals(30, testFood.getWeight());
    }

}
