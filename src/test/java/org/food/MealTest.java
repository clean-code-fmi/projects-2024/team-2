package org.food;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MealTest {

    private final Food testFood = new Food("Apple", "unit", 10, 20, 30, 4);

    @Test
    void whenRenamingMealExpectToBeRenamed() {
        Meal meal = new Meal("Old name");
        meal.rename("New name");
        Assertions.assertEquals(meal.getName(), "New name");
    }

    @Test
    void afterAddingFoodInMealExpectToBeContained() {
        Meal meal = new Meal("Test name");
        meal.addFood(testFood);
        Assertions.assertTrue(meal.isFoodPresent(testFood.getName()));
    }

    @Test
    void whenCheckingWhetherFoodWhichIsNotPresentExistsExpectFalse() {
        Meal meal = new Meal("Test name");
        meal.addFood(testFood);
        Assertions.assertFalse(meal.isFoodPresent("NotExist"));
    }

    @Test
    void whenCheckingWhetherFoodExistsInEmptyMealExpectFalse() {
        Meal meal = new Meal("Test name");
        Assertions.assertFalse(meal.isFoodPresent("NotExist"));
    }

    @Test
    void whenGettingFoodWhichExistsExpectToGetExactlyTheWantedFood() {

        Meal meal = new Meal("Test name");

        meal.addFood(testFood);
        Food food = meal.getFood(testFood.getName());

        Assertions.assertEquals(testFood.getName(), food.getName(),
                "Obtained food has changed name");
        Assertions.assertEquals(testFood.getDosage(), food.getDosage(),
                "Obtained food has changed dosage");
        Assertions.assertEquals(testFood.getCalories(), food.getCalories(),
                "Obtained food has changed calories");
        Assertions.assertEquals(testFood.getFats(), food.getFats(),
                "Obtained food has changed fats");
        Assertions.assertEquals(testFood.getWeight(),
                food.getWeight(),
                "Obtained food has changed weight (in grams)");
        Assertions.assertEquals(1, meal.getAllFoods().size(),
                "Unexpectedly deleted or added an item in food database");
    }

    @Test
    void whenGettingFoodWhichDoesNotExistExpectToGetNull() {
        Meal meal = new Meal("Test name");
        Assertions.assertNull(meal.getFood("NotExist"));
    }

    @Test
    void whenDeletingFoodWhichDoesNotExistExpectToGetNullAndUnchangedMap() {

        Meal meal = new Meal("Test name");
        meal.addFood(testFood);

        Food deletedFood = meal.removeFood("Not exist");

        Assertions.assertNull(deletedFood);
        Assertions.assertEquals(meal.getAllFoods().size(), 1);
    }

    @Test
    void whenDeletingFoodWhichExistsExpectToGetFoodReferenceAndReducedSize() {

        Meal meal = new Meal("Test name");
        meal.addFood(testFood);

        Food food = meal.removeFood(testFood.getName());

        Assertions.assertEquals(testFood.getName(), food.getName(),
                "Obtained food has changed name");
        Assertions.assertEquals(testFood.getDosage(), food.getDosage(),
                "Obtained food has changed dosage");
        Assertions.assertEquals(testFood.getCalories(), food.getCalories(),
                "Obtained food has changed calories");
        Assertions.assertEquals(testFood.getFats(), food.getFats(),
                "Obtained food has changed fats");
        Assertions.assertEquals(testFood.getWeight(),
                food.getWeight(),
                "Obtained food has changed weight (in grams)");
        Assertions.assertEquals(0, meal.getAllFoods().size(),
                "Deletion of food in database did not reduce count of items");
    }



}
