package org.cli;
import org.account.Account;
import org.utils.Height;
import org.utils.HeightUnit;

import java.util.InputMismatchException;
import java.util.Scanner;

public final class RegistrationMenu {
    static final String CENTIMETERS_STRING = "cm";
    private final Scanner scanner;

    public RegistrationMenu(Scanner scanner) {
        this.scanner = scanner;
    }

    public Account getRegistration() {
        String[] names = getName();
        int age = getAge();
        float weight = getWeight();
        Height height = getHeight();

        return new Account(names, age, weight, height);
    }

    public String[] getName() {
        while (true) {
            System.out.println("Please provide your name in the"
                    + " following format: <first_name> <last_name>");
            String scannedStr = scanner.nextLine();
            if (!scannedStr.contains(" ")) {
                System.out.println("Input not in the correct format: \n"
                        + "Expected format is <first_name>[string]"
                        + " <last_name>[string]");
            } else if (scannedStr.length() < 3) {
                System.out.println("Name is too short. Try again.");
            } else {
                return scannedStr.split(" ");
            }
        }
    }

    public int getAge() {
        while (true) {
            System.out.println("Please provide your age: ");
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Number not in correct format."
                        + " Please try again.");
            }
        }
    }

    public float getWeight() {
        while (true) {
            System.out.println("Please provide your weight in"
                    + " kilograms: [Example: 50.55]");
            try {
                return scanner.nextFloat();
            } catch (InputMismatchException e) {
                System.out.println("Value not in correct format."
                        + " Please try again.");
            }
        }
    }

    public Height getHeight() {
        while (true) {
            System.out.println("Please provide your height:"
                    + " [Example for feet: 5'8\"; Example for cm: 175cm]");
            String heightInfo = scanner.nextLine();
            try {
                if (heightInfo.contains(CENTIMETERS_STRING)) {
                    return new Height(
                            HeightUnit.CENTIMETERS,
                            Float.parseFloat(heightInfo.split("c")[0].trim()));
                } else {
                    String[] feetInches = heightInfo.split("['\"]");
                    return new Height(
                            HeightUnit.INCHES,
                            Integer.parseInt(feetInches[0].trim()) * 12f
                                    + Integer.parseInt(feetInches[1].trim())
                    );
                }
            } catch (NumberFormatException e) {
                System.out.println("Input not in correct format."
                        + " Please try again.");
            }
        }
    }
}
