package org.food;

import java.util.HashMap;
import java.util.Map;
import java.util.Hashtable;

public final class FoodDatabase {

    private final Map<String, Food> foods;

    public FoodDatabase() {
        foods = new Hashtable<>();
    }

    public void addFood(Food food) {
        foods.put(food.getName(), food);
    }

    public Food getFood(String name) {
        return foods.get(name);
    }

    public boolean foodExists(String name) {
        return foods.containsKey(name);
    }

    public Food removeFood(String name) {
        return foods.remove(name);
    }

    public Map<String, Food> getAllFoods() {
        return new HashMap<>(foods);
    }

}
