package org.food;

import java.util.HashMap;
import java.util.Map;

public final class Meal {

    private String name;
    private final Map<String, Food> foods;

    public Meal(String name) {
        this.name = name;
        this.foods = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void rename(String newName) {
        this.name = newName;
    }

    public void addFood(Food food) {
        foods.put(food.getName(), food);
    }

    public Food getFood(String foodName) {
        return foods.get(foodName);
    }

    public Food removeFood(String foodName) {
        return foods.remove(foodName);
    }

    public boolean isFoodPresent(String foodName) {
        return foods.containsKey(foodName);
    }

    public Map<String, Food> getAllFoods() {
        return new HashMap<>(foods);
    }

    public double getCalories() {
        return foods.values()
                .stream()
                .mapToDouble(Food::getTotalCalories)
                .sum();
    }

    public double getFats() {
        return foods.values().stream().mapToDouble(Food::getTotalFats).sum();
    }

}
