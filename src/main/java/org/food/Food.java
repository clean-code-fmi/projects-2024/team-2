package org.food;


public final class Food {


    /** Food name */
    private String name;

    /** Describes how would one refer to a serving of that food. */
    private String dosage;

    /** Total calories per 1 quantity of the food */
    private double calories;

    /** Total fats per 1 quantity of the food */
    private double fats;

    /** Total weight (in grams) per 1 quantity of the food object */
    private double weight;

    /** Quantity of said food (if not applicable, default to 1)  */
    private int quantity;

    public Food(String name,
                String dosage,
                double calories,
                double fats,
                double weight,
                int quantity) {
        setName(name);
        setDosage(dosage);
        setCalories(calories);
        setFats(fats);
        setWeight(weight);
        setQuantity(quantity);
    }

    public Food(String name,
                String dosage,
                double calories,
                double fats,
                double weightInGrams) {
        this(name, dosage, calories, fats, weightInGrams, 1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public double getCalories() {
        return calories;
    }

    public double getTotalCalories() {
        return this.calories * this.quantity;
    }

    public void setCalories(double calories) {

        if (calories < 0) {
            throw new IllegalArgumentException("Calories cannot be negative");
        }

        this.calories = calories;
    }

    public double getFats() {
        return fats;
    }

    public double getTotalFats() {
        return this.fats * this.quantity;
    }

    public void setFats(double fats) {

        if (fats < 0) {
            throw new IllegalArgumentException("Fats cannot be negative");
        }

        this.fats = fats;
    }

    public double getWeight() {
        return weight;
    }

    public double getTotalWeight() {
        return this.weight * this.quantity;
    }

    public void setWeight(double weight) {

        if (weight < 0) {
            throw new IllegalArgumentException("Fats cannot be negative");
        }

        this.weight = weight;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {

        if (quantity <= 0) {
            throw new IllegalArgumentException(
                    "Quantity cannot be negative or 0");
        }

        this.quantity = quantity;
    }
}
