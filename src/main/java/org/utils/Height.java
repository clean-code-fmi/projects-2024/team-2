package org.utils;

public final class Height {
    private final HeightUnit unit;
    private final float value;

    public Height(HeightUnit unit, float value) {
        this.unit = unit;
        this.value = value;
    }

    public HeightUnit getHeightUnit() {
        return this.unit;
    }

    public float getHeightValue() {
        return this.value;
    }
}
