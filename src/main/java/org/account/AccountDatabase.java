package org.account;

import java.util.ArrayList;
import java.util.List;

public class AccountDatabase {
    private final List<Account> accounts = new ArrayList<>();

    public final void addAccount(Account toAdd) {
        this.accounts.add(toAdd);
    }

    public final List<Account> getAccounts() {
        return this.accounts;
    }
}
