package org.account;

import org.cli.RegistrationMenu;

public final class AccountOrchestrator {
    private final AccountDatabase db;
    private final RegistrationMenu menu;

    public AccountOrchestrator(AccountDatabase db, RegistrationMenu menu) {
        this.db = db;
        this.menu = menu;
    }

    public void register() {
        Account toAdd = menu.getRegistration();
        db.addAccount(toAdd);
    }
}
