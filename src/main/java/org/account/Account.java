package org.account;

import org.utils.Height;

public final class Account {
    private String firstName;
    private String lastName;
    private int age;
    private float weight;
    private Height height;

    public Account() {
        this.firstName = null;
        this.lastName = null;
        this.age = 0;
        this.weight = 0f;
        this.height = null;
    }

    public Account(String[] fullName,
                   int age,
                   float weight,
                   Height height) {
        this.setName(fullName);
        this.setAge(age);
        this.setWeight(weight);
        this.setHeight(height);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setName(String[] fullName) {
        setFirstName(fullName[0]);
        setLastName(fullName[1]);
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public float getWeight() {
        return weight;
    }

    public Height getHeight() {
        return height;
    }
}
